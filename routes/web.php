<?php

use Symfony\Component\HttpClient\HttpClient;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $version = $router->app->version();
    return view( "index", ['name' => "stefan"]);
});

$router->get('/fortune', function () use ($router) {
    $client = HttpClient::create();
    $rand = random_int(1,1000);
    $response = $client->request('GET', "https://fortunecookieapi.herokuapp.com/v1/fortunes?limit=1&skip=$rand");

    $statusCode = $response->getStatusCode();
    $result = $response->toArray();
     
    return view( "fortune", ['index' => $rand, 'message' => $result[0]['message'] ]);
});